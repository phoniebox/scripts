#!/usr/bin/env python

import commands
import RPi.GPIO as GPIO

output = commands.getoutput('ps -aux | grep -v grep | grep RFID')

RED_LED = 37
YELLOW_LED = 8
GREEN_LED = 36

GPIO.setmode(GPIO.BOARD)
GPIO.setup(RED_LED, GPIO.OUT)
GPIO.setup(YELLOW_LED, GPIO.OUT)
GPIO.setup(GREEN_LED, GPIO.OUT)

if(len(output) > 0):
    GPIO.output(GREEN_LED, GPIO.HIGH)
    GPIO.output(RED_LED, GPIO.LOW)
    GPIO.output(YELLOW_LED, GPIO.LOW)
else:
    GPIO.output(GREEN_LED, GPIO.LOW)
    GPIO.output(RED_LED, GPIO.HIGH)
    GPIO.output(YELLOW_LED, GPIO.LOW)

