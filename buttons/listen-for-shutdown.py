#!/usr/bin/env python


import RPi.GPIO as GPIO
import subprocess


GPIO.setmode(GPIO.BCM)
GPIO.setup(3, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.wait_for_edge(3, GPIO.FALLING)


YELLOW_LED = 8

GPIO.setmode(GPIO.BOARD)
GPIO.setup(YELLOW_LED, GPIO.OUT)
GPIO.output(YELLOW_LED, GPIO.HIGH)

subprocess.call(['shutdown', '-h', 'now'], shell=False)